package dev.scottlaing.composebeta

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Add
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun MainContent(
    stories: List<NewsStoryState>,
    addStory: (String) -> Unit,
    storySelected: (String) -> Unit
) {
    LazyColumn {
        items(stories) { story ->
            NewsStory(story, storySelected)
            Divider(color = Color.Black)
        }
        item {
            Spacer(modifier = Modifier.padding(bottom = 64.dp))
        }
    }
    Box(
        Modifier.fillMaxSize(),
        contentAlignment = Alignment.BottomEnd
    ) {
        FloatingActionButton(
            modifier = Modifier.padding(all = 16.dp),
            onClick = { addStory("New Story (#${(Math.random() * 1000).toInt()})") }
        ) {
            Icon(Icons.Outlined.Add, contentDescription = "")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun MainContentPreview() {
    MainContent(
        stories = listOf(
            NewsStoryState("Android Projects 101", true),
            NewsStoryState("Android Layouts with Jetpack Compose", false)
        ),
        addStory = { },
        storySelected = { }
    )
}