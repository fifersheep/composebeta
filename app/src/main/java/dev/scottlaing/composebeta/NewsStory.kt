package dev.scottlaing.composebeta

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun NewsStory(story: NewsStoryState, onClick: (String) -> Unit) {
    MaterialTheme {
        val typography = MaterialTheme.typography
        val backgroundColor = if (story.isSelected) Color.Gray else MaterialTheme.colors.background
        Column(
            modifier = Modifier
                .clickable { onClick(story.title) }
                .background(color = backgroundColor)
        ) {
            Image(
                modifier = Modifier
                    .height(180.dp)
                    .fillMaxWidth()
                    .padding(16.dp)
                    .clip(shape = RoundedCornerShape(4.dp)),
                contentDescription = null,
                painter = painterResource(R.drawable.header),
                contentScale = ContentScale.Crop
            )

            Text(
                story.title,
                style = typography.h6,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.padding(horizontal = 16.dp)
            )
            Text(
                "Davenport, California",
                style = typography.body2,
                modifier = Modifier.padding(horizontal = 16.dp)
            )
            Text(
                "December 2018",
                style = typography.body2,
                modifier = Modifier.padding(
                    start = 16.dp,
                    end = 16.dp,
                    bottom = 16.dp
                )
            )
        }
    }
}

@Preview(showBackground = true, name = "News Story, deselected")
@Composable
fun NewsStoryDeselectedPreview() {
    NewsStory(NewsStoryState("A day wandering through the sandhills " +
            "in Shark Fin Cove, and a few of the " +
            "sights I saw", false)
    ) {}
}

@Preview(showBackground = true, name = "News Story, selected")
@Composable
fun NewsStoryPreview() {
    NewsStory(NewsStoryState("A day wandering through the sandhills " +
            "in Shark Fin Cove, and a few of the " +
            "sights I saw", true)
    ) {}
}