package dev.scottlaing.composebeta

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import dev.scottlaing.composebeta.ui.theme.ComposeBetaTheme

@Composable
fun App(content: @Composable () -> Unit) {
    ComposeBetaTheme {
        Surface(color = MaterialTheme.colors.background) {
            content()
        }
    }
}

@Preview(showBackground = true)
@Composable
fun AppPreview() {
    App {
        MainContent(
            stories = listOf(NewsStoryState("Title", false)),
            addStory = { },
            storySelected = { }
        )
    }
}