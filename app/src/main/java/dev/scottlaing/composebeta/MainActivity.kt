package dev.scottlaing.composebeta

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.tooling.preview.Preview

data class NewsStoryState(
    val title: String,
    val isSelected: Boolean
)

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val titles = rememberSaveable {
                mutableStateOf(
                    listOf<NewsStoryState>()
                )
            }

            App {
                MainContent(
                    stories = titles.value,
                    addStory = { title ->
                        titles.value = titles.value + listOf(
                            NewsStoryState(title, false)
                        )
                    },
                    storySelected = { title ->
                        titles.value = titles.value.map {
                            it.copy(isSelected = it.title == title)
                        }
                    }
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun MainActivityPreview() {
    App {
        MainContent(
            stories = listOf(
                NewsStoryState("Android Projects 101", true),
                NewsStoryState("Android Layouts with Jetpack Compose", false)
            ),
            addStory = { },
            storySelected = { }
        )
    }
}
